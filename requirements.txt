Django==2.1.3
jsonpickle==1.0
python-dateutil==2.7.5
pytz==2018.7
six==1.11.0
